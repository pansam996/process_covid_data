import os
import queue
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt

node_av = []
node_neighbor = {}
Record_D = []
D_cost = []

def RIP(root,S,OS_T):
    RR = []
    v = root
    met = False
    t_end = OS_T - 1
    for t in range(OS_T):
        N = []
        for u in node_neighbor[v].keys():
            p = random.uniform(0,1)
            if p <= node_neighbor[v][u]:
                N.append(u)
        if N == []:
            break
        _u_idx = random.randint(0,len(N) - 1)
        _u = N[_u_idx]
        if _u in S:
            met = True
            t_end = t
            break
        else:
            RR.append((_u,t))
            v = _u
    if met == True:
        for idx in range(len(RR)):
            RR[idx] = (RR[idx][0], t_end - RR[idx][1])
        
    else:
        RR = []

    return RR

def cover(M,U):
    count_list = []
    for u_idx in range(len(U)):
        count = 0
        nodes = [ int(n) for n in U[u_idx].split(' ')[1].split('_')[4][:-1].split(',')]
        for m in M:
            if m != []:
                for tup in m:
                    if tup[0] in nodes:
                        count += 1
                        break
        count_list.append((u_idx,count))
    
    return count_list

def gowalla_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R):
    print("Generate OSgraph...")

    ## 處理graph 讀資料進來，存到structure
    with open('./_1_gowalla/graph/graph_1_T=1.txt','r') as f:
        lines = f.readlines()
    f.close()

    file_name = './_1_gowalla/graph/OSgraph_1_T='+ str(OS_T) + '_lv='+ str(OS_lv) + '_b='+ str(OS_budget)+ '_R='+ str(OS_R) +'.txt'

    
    with open(file_name,'w') as f:
        for i in lines:
            data_type = i.split(' ')[0]
            data = i.split(' ')[1]
            if data_type != 'X':
                f.write(i)
            if data_type == 'n':
                DATA = data.split(',')
                no = int(DATA[0])
                a_v = float(DATA[10][:-2])
                node_av.append(a_v)
                # init node_neighbor
                node_neighbor[no] = {}
            if data_type == 'e':
                DATA = data.split(',')
                n1 = int(DATA[0])
                n2 = int(DATA[1])
                prob = float(DATA[2][:-2])
                node_neighbor[n1][n2] = prob
            if data_type == 'X':
                DATA = data.split('_')
                day = int(DATA[0])
                cost = int(DATA[1])
                _lv = int(DATA[2])
                if _lv == OS_lv:
                    Record_D.append(i)
                    users = DATA[4].split(',')
                    D_cost.append(cost)

    ## OS-IP
    M = []
    for r in range(OS_R):
        root = random.randint(0,len(node_av) - 1)
        S = []
        for v in range(len(node_av)):
            p = random.uniform(0,1)
            if p <= node_av[v]:
                S.append(v)
        RIP_result = RIP(root,S,OS_T)
        M.append(RIP_result)

    U = []
    u_cost = []
    for t in range(OS_T):
        u_cost += D_cost
        for d in Record_D:
            U.append(d[:2] + str(t) + d[3:])

    count_list = cover(M,U)
    count_list.sort(key=lambda tup:tup[1], reverse = True)

    S = []
    while OS_budget > 0 and count_list != []:
        Dtop = count_list[0]
        Dtop_cost = u_cost[Dtop[0]]
        while Dtop_cost > OS_budget and U != []:
            count_list.remove(count_list[0])
            if count_list != []:
                Dtop = count_list[0]
                Dtop_cost = u_cost[Dtop[0]]
            else:
                break
        if Dtop_cost > OS_budget :
            break
        if count_list == []:
            break
        S.append(U[Dtop[0]])
        OS_budget -= Dtop_cost
        count_list.remove(count_list[0])

    with open(file_name,'a') as f:
        for s in S:
            f.write(s)
    
    f = open(file_name, "r")
    contents = f.readlines()
    f.close()

    tmp = contents[0].split(',')

    contents[0] = tmp[0] + "," + tmp[1] + "," + str(len(S)) + '\n'

    f = open(file_name, "w")
    contents = "".join(contents)
    f.write(contents)
    f.close()
    print('Done')