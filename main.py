from _1_gowalla.gowalla_generate_graph import gowalla_generate_graph_1
from _1_gowalla.gowalla_generate_groups import gowalla_generate_groups,gowalla_gnenrate_plot_graphs
from _1_gowalla.gowalla_generate_RLgraph import gowalla_generate_RLgraph
from _1_gowalla.gowalla_generate_OSgraph import gowalla_generate_OSgraph
from _2_foursquare.foursquare_generate_graph import foursquare_generate_graph_2
from _2_foursquare.foursquare_generate_groups import foursquare_generate_groups,foursquare_gnenrate_plot_graphs
from _2_foursquare.foursquare_generate_RLgraph import foursquare_generate_RLgraph
from _2_foursquare.foursquare_generate_OSgraph import foursquare_generate_OSgraph
from _3_Brightkite.Brightkite_generate_graph import Brightkite_generate_graph_3
from _3_Brightkite.Brightkite_generate_groups import Brightkite_generate_groups,Brightkite_gnenrate_plot_graphs
from _3_Brightkite.Brightkite_generate_RLgraph import Brightkite_generate_RLgraph
from _3_Brightkite.Brightkite_generate_OSgraph import Brightkite_generate_OSgraph
from _4_HaslemereNetwork.HaslemereNetwork_generate_graph import HaslemereNetwork_generate_graph_4
from _4_HaslemereNetwork.HaslemereNetwork_generate_groups import HaslemereNetwork_generate_groups,HaslemereNetwork_gnenrate_plot_graphs
from _4_HaslemereNetwork.HaslemereNetwork_generate_RLgraph import HaslemereNetwork_generate_RLgraph
from _4_HaslemereNetwork.HaslemereNetwork_generate_OSgraph import HaslemereNetwork_generate_OSgraph
from _4_HaslemereNetwork.HaslemereNetwork_generate_OPTgraph import HaslemereNetwork_generate_OPTgraph
from _NYC.NYC_generate_graph import NYC_generate_graph_NYC
from _NYC.NYC_generate_groups import NYC_generate_groups,NYC_gnenrate_plot_graphs
from _NYC.NYC_generate_RLgraph import NYC_generate_RLgraph
from _NYC.NYC_generate_OSgraph import NYC_generate_OSgraph
from _TKY.TKY_generate_graph import TKY_generate_graph_TKY
from _TKY.TKY_generate_groups import TKY_generate_groups,TKY_gnenrate_plot_graphs
from _TKY.TKY_generate_RLgraph import TKY_generate_RLgraph
from _TKY.TKY_generate_OSgraph import TKY_generate_OSgraph
from _pokec.pokec_generate_groups import pokec_generate_groups,pokec_gnenrate_plot_graphs
from _pokec.pokec_generate_graphs import pokec_generate_graph_pokec
from _pokec.pokec_generate_RLgraph import pokec_generate_RLgraph
from _pokec.pokec_generate_OSgraph import pokec_generate_OSgraph
##-------------------------- Graph Parameters --------------------------##
## T
T = [1]

##------------- Node -------------##
R = 0.55
Ct = 0.57
S = 0.13
Cr = 0.02
HI = 0.03
HA = 0.02
HT = 0.02
D = 0.01
a_v_mode = 0 # 0 : uniform_distribution / 1 : normal_distribution
mean = 0.5
var = 0.05
node_parameter = [R,Ct,S,Cr,HI,HA,HT,D,a_v_mode,mean,var]
##--------------------------------##

##------------- Groups -------------##
groups = 100
basic_num = 1000
least_num_of_users = 50
lv = 3
remove_groups_less_than_num = -1
##----------------------------------##
##-------------------------------------------------------------------------##


'''
- EXAMPLE :

1.
gowalla_generate_groups(groups,least_num_of_users,basic_num)
gowalla_generate_graph_1(T,groups,node_parameter,lv,remove_groups_less_than_num)

2.
foursquare_generate_groups(groups,least_num_of_users,basic_num)
foursquare_generate_graph_2(T,groups,node_parameter,lv,remove_groups_less_than_num)

3.
Brightkite_generate_groups(groups,least_num_of_users,basic_num)
Brightkite_generate_graph_3(T,groups,node_parameter,lv,remove_groups_less_than_num)

4.
HaslemereNetwork_generate_groups(groups,least_num_of_users,basic_num)
HaslemereNetwork_generate_graph_4(T,groups,node_parameter,lv,remove_groups_less_than_num)

5.
NYC_generate_groups(groups,least_num_of_users,basic_num)
NYC_generate_graph_NYC(T,groups,node_parameter,lv,remove_groups_less_than_num)

6.
TKY_generate_groups(groups,least_num_of_users,basic_num)
TKY_generate_graph_TKY(T,groups,node_parameter,lv,remove_groups_less_than_num)

7.
pokec_generate_groups(groups,least_num_of_users,basic_num)
pokec_generate_graph_pokec(T,groups,node_parameter,lv,remove_groups_less_than_num)

'''

##-------------------------- Graph Generates ----------------------------##

# pokec_generate_graph_pokec(T,groups,node_parameter,lv,remove_groups_less_than_num)

##-----------------------------------------------------------------------##

############################################################################

##---------------------------RL parameters----------------------------##
RL_T = 10
RL_lv = 3 # graph lv {1:0.2 ,2:0.8, 3:1}
budget = 100
level_convert = {1:0.2 ,2:0.8, 3:1}
##--------------------------------------------------------------------##
'''
Example
1.
gowalla_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

2.
foursquare_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

3.
Brightkite_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

4.
HaslemereNetwork_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

5.
NYC_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

6.
TKY_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

7.
pokec_generate_RLgraph(RL_T,RL_lv,budget,level_convert)

'''
##------------------------- RLgraph Generates -------------------------##

pokec_generate_RLgraph(RL_T,RL_lv,budget)

##---------------------------------------------------------------------##

############################################################################

##-------------------------OS graph Parameters ------------------------##
OS_T = 10
OS_lv = 2
OS_budget = 1000
OS_R = 100
##--------------------------------------------------------------------##

'''
Example:
1.
gowalla_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

2.
foursquare_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

3.
Brightkite_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

4.
HaslemereNetwork_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

5.
NYC_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

6.
TKY_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

7.
pokec_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

'''
##---------------------------OSgraph Generates --------------------------##

pokec_generate_OSgraph(OS_T,OS_lv,OS_budget,OS_R)

##-----------------------------------------------------------------------##

#############################################################################

##--------------------------OPTgraph Parameters-------------------------##
OPT_T = [1,2,3]
OPT_groups = 3
OPT_budget = [1,3,5,10]
OPT_lv = 2 # level_convert = {1:0.5 ,2:1}
OPT_least_num_of_users = 10

R = 0.55
Ct = 0.57
S = 0.13
Cr = 0.02
HI = 0.03
HA = 0.02
HT = 0.02
D = 0.01
a_v_mode = 0 # 0 : uniform_distribution / 1 : normal_distribution
mean = 0.5
var = 0.05
OPT_node_parameter = [R,Ct,S,Cr,HI,HA,HT,D,a_v_mode,mean,var]


## Generate OPTgraph
# HaslemereNetwork_generate_OPTgraph(OPT_T, OPT_groups, OPT_budget, OPT_node_parameter, OPT_lv, OPT_least_num_of_users)