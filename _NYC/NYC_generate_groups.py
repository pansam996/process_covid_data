import os
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt

def haversine(lon1, lat1, lon2, lat2): # 經度1，緯度1，經度2，緯度2 （十進制）
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # 將十進制轉為弧度
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine公式
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371 # 地球平均半徑，單位為公里
    return c * r * 1000

def NYC_generate_groups(groups,least_num_of_users,basic_num):
    #### Read the user_user_distance data
    path = './_NYC/user_user_distance_NYC.txt'

    data = pd.read_csv(path,header=None,sep=',')

    # column info
    time = data[:][0] # timeslot
    id_1 = data[:][1] # userid 1
    id_2 = data[:][2] # userid 2
    distance = data[:][3] # distance

    #### add the match node and edge
    match_nodes = []
    edge_list = []
    threshold = 500
    for i in range(len(data)):
        if distance[i] <= threshold:
            match_nodes.append(id_1[i])
            match_nodes.append(id_2[i])
            edge_list.append([id_1[i],id_2[i]])

    G = nx.Graph()
    # add node
    for i in range(len(match_nodes)):
        G.add_node(match_nodes[i])

    # add edge
    for i in range(len(edge_list)):
        G.add_edge(edge_list[i][0],edge_list[i][1])

    all_edge = list(G.edges)
    all_edge.sort()

    all_node = list(G.nodes)
    all_node.sort()

    #### Calculate the max radius
    user_center_data = pd.read_csv('./_NYC/user_center_NYC.txt',header=None,sep=',')
    user_x = np.array(user_center_data[:][1])
    user_y = np.array(user_center_data[:][2])
    #### find the farest x_value on +x axis and -x axis
    x_max = np.amax(user_x)
    x_min = np.amin(user_x)
    #### find the farest y_value on +y axis and -y axis
    y_max = np.amax(user_y)
    y_min = np.amin(user_y)

    length = round(haversine(x_max,y_max,x_min,y_min),2)
    max_radius = round(length/2,2)
    print('Diagonal length : ',length)
    print('Max radius : ',max_radius)

    with open('./_NYC/NYC_100_groups','w') as f:
        for idx in range(groups):
            # empty set
            sd = set()
            eta = -1

            while(len(sd) <= least_num_of_users):
                ## random radius by "normal distrubtion"
                radius = np.random.normal(3800,scale=200)
                ## random user be the center
                centers = np.random.randint(0,G.number_of_nodes() - 1)
                # find the user that in the center range
                for i in range(len(all_node)):
                    center_x = user_center_data[1][centers]
                    center_y = user_center_data[2][centers]
                    compare_x = user_center_data[1][i]
                    compare_y = user_center_data[2][i]
                    l = haversine(center_x, center_y ,compare_x,compare_y )
                    if l <= radius:
                        sd.add(i)
            print("Group_", idx+1," :",len(sd))
            cost = math.ceil(len(sd) / basic_num)
            f.write('{}_{}\n'.format(cost, ",".join(str(i) for i in sd)))
    f.close()

def NYC_gnenrate_plot_graphs():
    user_center_data = pd.read_csv('./_NYC/user_center_NYC.txt',header=None,sep=',')
    user_x = np.array(user_center_data[:][1])
    user_y = np.array(user_center_data[:][2])
    plt.figure(figsize=(8,8))
    plt.scatter(user_x,user_y)
    plt.savefig('./_NYC/NYC.png')