import os
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt

def pokec_generate_groups(groups,least_num_of_users,basic_num):
	print("Generating groups...")
	#### read the edge data
	edge_list = []
	edge_data = pd.read_csv('./_pokec/soc-pokec-relationships.txt',header=None,sep='\t')
	G = nx.Graph()

	for i in range(len(edge_data)):
		G.add_edge(edge_data[0][i], edge_data[1][i])

	all_edge = list(G.edges)
	all_edge.sort()

	all_node = list(G.nodes)
	all_node.sort()

	# generate 100 groups
	with open('./_pokec/pokec_100_groups','w') as f:
		for group_num in range(groups):
			user_num = random.randint(least_num_of_users,5000)
			print("Group_", group_num," :",user_num)
			users = np.random.randint(0,G.number_of_nodes()-1,size = user_num)
			cost = int(user_num / basic_num)
			f.write('{}_{}\n'.format(cost, ",".join(str(i) for i in users)))


def pokec_gnenrate_plot_graphs():
	print("Generating plot_graph...")
	#### read the edge data
	edge_list = []
	edge_data = pd.read_csv('./_pokec/soc-pokec-relationships.txt',header=None,sep='\t')
	G = nx.Graph()

	for i in range(len(edge_data)):
		G.add_edge(edge_data[0][i], edge_data[1][i])

	all_edge = list(G.edges)
	all_edge.sort()

	all_node = list(G.nodes)
	all_node.sort()

	plt.figure(1,figsize=(8,8))
	nx.draw_networkx_nodes(G,pos=nx.random_layout(G),node_size=20)
	plt.savefig('./_pokec/pokec.png')