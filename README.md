# covid_data_process
- 整理打卡資料形成graph

## `main.py` 說明
- 生成graph
- 生成groups
- 生成OSgraph
- 生成RLgraph
- 生成OPTgrapg
---
- 參數調整區
```python
## T
T = [1,10,20,30,40]

##------------- Node -------------##
R = 0.55
Ct = 0.57
S = 0.13
Cr = 0.02
HI = 0.03
HA = 0.02
HT = 0.02
D = 0.01
node_parameter = [R,Ct,S,Cr,HI,HA,HT,D]
##--------------------------------##

##------------- Groups -------------##
groups = 100
basic_num = 100
least_num_of_users = 500
lv = 3
remove_groups_less_than_num = -1
##----------------------------------##
```
- 檔案生成範例
```python
### 生成gowalla_groups
gowalla_generate_groups(groups,least_num_of_users,basic_num)
### 生成gowalla_graphs
gowalla_generate_graph_1(T,groups,node_parameter,lv,remove_groups_less_than_num)
```

- 執行`main.py`
```python
python3 main.py
```
## File Structure
```
.
├── README.md
├── _1_gowalla
│   ├── correct_friend_ship.csv
│   ├── correct_user_center.csv
│   ├── gowalla.png
│   ├── gowalla_100_groups
│   ├── gowalla_generate_OSgraph.py
│   ├── gowalla_generate_RLgraph.py
│   ├── gowalla_generate_graph.py
│   ├── gowalla_generate_groups.py
│   └── graph
├── _2_foursquare
│   ├── correct_friend_ship.csv
│   ├── correct_user_center.csv
│   ├── foursquare.png
│   ├── foursquare_100_groups
│   ├── foursquare_generate_OSgraph.py
│   ├── foursquare_generate_RLgraph.py
│   ├── foursquare_generate_graph.py
│   ├── foursquare_generate_groups.py
│   ├── graph
│   └── user_center_2.txt
├── _3_Brightkite
│   ├── Brightkite.png
│   ├── Brightkite_100_groups
│   ├── Brightkite_edges.txt
│   ├── Brightkite_generate_OSgraph.py
│   ├── Brightkite_generate_RLgraph.py
│   ├── Brightkite_generate_graph.py
│   ├── Brightkite_generate_groups.py
│   ├── graph
│   └── user_center_3.txt
├── _4_HaslemereNetwork
│   ├── HaslemereNetwork.png
│   ├── HaslemereNetwork_100_groups
│   ├── HaslemereNetwork_generate_OPTgraph.py
│   ├── HaslemereNetwork_generate_OSgraph.py
│   ├── HaslemereNetwork_generate_RLgraph.py
│   ├── HaslemereNetwork_generate_graph.py
│   ├── HaslemereNetwork_generate_groups.py
│   ├── Kissler_DataS1.csv
│   └── graph
├── _NYC
│   ├── NYC.png
│   ├── NYC_100_groups
│   ├── NYC_generate_OSgraph.py
│   ├── NYC_generate_RLgraph.py
│   ├── NYC_generate_graph.py
│   ├── NYC_generate_groups.py
│   ├── graph
│   ├── user_center_NYC.txt
│   └── user_user_distance_NYC.txt
├── _TKY
│   ├── TKY.png
│   ├── TKY_100_groups
│   ├── TKY_generate_OSgraph.py
│   ├── TKY_generate_RLgraph.py
│   ├── TKY_generate_graph.py
│   ├── TKY_generate_groups.py
│   ├── graph
│   ├── user_center_TKY.txt
│   └── user_user_distance_TKY.txt
├── _pokec
│   ├── graph
│   ├── pokec.png
│   ├── pokec_100_groups
│   ├── pokec_generate_OSgraph.py
│   ├── pokec_generate_RLgraph.py
│   ├── pokec_generate_graphs.py
│   ├── pokec_generate_groups.py
│   └── soc-pokec-relationships.txt
└── main.py
```
