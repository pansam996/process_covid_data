import os
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt


def Brightkite_generate_graph_3(T, groups, node_parameter,lv,remove_groups_less_than_num):
    t_len = len(T)
    #### read the edge data
    edge_list = []
    edge_data = pd.read_csv('./_3_Brightkite/Brightkite_edges.txt',header=None,sep='\t')

    for i in range(len(edge_data)):
        edge_list.append([edge_data[0][i], edge_data[1][i]])

    G = nx.Graph()
    # add edge
    for i in range(len(edge_list)):
        G.add_edge(edge_list[i][0],edge_list[i][1])

    print('Edge number :',G.number_of_edges(),'\nNode number :',G.number_of_nodes())

    all_edge = list(G.edges)
    all_edge.sort()

    all_node = list(G.nodes)
    all_node.sort()

    # a_v probability generate
    a_v_mode = node_parameter[8]
    a_v = []
    if a_v_mode:
        mean = node_parameter[9]
        var = node_parameter[10]
        for i in range(len(all_edge)):
            prob = round(np.random.normal(mean,scale=var),4)
            a_v.append(prob)

    else:
        for i in range(len(all_edge)):
            prob = round(random.uniform(0,1),4)
            a_v.append(prob)

    for t_idx in range(t_len):
        print("Generating graph_",T[t_idx])
        random.seed(10)
        np.random.seed(10)
        ##### graph.txt
        with open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt','w') as f:
            f.write('{} {},{},{}\n'.format('g',G.number_of_nodes(),G.number_of_edges(),T[t_idx] * groups * lv))
        f.close()

        #### Node
        with open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt','a') as f:
            for i in all_node:
                node_type = random.randint(0,3)
                R = node_parameter[0]
                Ct = node_parameter[1]
                S = node_parameter[2]
                Cr = node_parameter[3]
                HI = node_parameter[4]
                HA = node_parameter[5]
                HT = node_parameter[6]
                D = node_parameter[7]
                f.write('{} {},{},{},{},{},{},{},{},{},{},{}\n'.format('n',i , node_type, R, Ct, S, Cr, HI, HA, HT, D,a_v[i]))
        f.close()

        #### Edge
        with open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt','a') as f:
            for i in range(len(all_edge)):
                prob = random.uniform(0,1)
                node1 = all_edge[i][0]
                node2 = all_edge[i][1]
                f.write('{} {},{},{}\n'.format('e', node1,node2,round(prob,3)))
        f.close()

        #read the 100 groups
        _100_groups = pd.read_csv('./_3_Brightkite/Brightkite_100_groups',header=None,sep='_')
        #### Group
        count_x = 0
        with open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt','a') as f:
            for d in range(T[t_idx]):
                for idx in range(len(_100_groups)):
                    g = _100_groups.iloc[idx][1]
                    cost = _100_groups.iloc[idx][0]
                    eta = -1
                    if remove_groups_less_than_num > 0:
                        if len(g.split(',')) > remove_groups_less_than_num:
                            for l in range(lv):
                                f.write('{} {}_{}_{}_{}_{}\n'.format('X', d, cost, l+1, eta, g))
                                count_x += 1
                        else:
                            continue
                    else:
                        for l in range(lv):
                            f.write('{} {}_{}_{}_{}_{}\n'.format('X', d, cost, l+1, eta, g))
        f.close()

        if remove_groups_less_than_num > 0:
            f = open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt', "r")
            contents = f.readlines()
            f.close()

            tmp = contents[0].split(',')

            contents[0] = tmp[0] + "," + tmp[1] + "," + str(count_x) + '\n'

            f = open('./_3_Brightkite/graph/graph_3'+ '_T=' + str(T[t_idx]) +'.txt', "w")
            contents = "".join(contents)
            f.write(contents)
            f.close()

