import os
import queue
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt


def Brightkite_generate_RLgraph(RL_T,RL_lv,budget,level_convert):
    print("Generate RLgraph...")

    ## 處理graph 讀資料進來，存到structure
    with open('./_3_Brightkite/graph/graph_3_T=1.txt','r') as f:
        lines = f.readlines()
    f.close()

    level_convert = level_convert
    node_data = {}
    Record_X = []
    Queue = []
    D_cost = []
    X_count = 0
    file_name = './_3_Brightkite/graph/RLgraph_3_T='+ str(RL_T) + '_lv='+ str(RL_lv) + '_b='+ str(budget) +'.txt'
    node_num = 0
    edge_num = 0
    with open(file_name,'w') as f:
        for i in lines:
            data_type = i.split(' ')[0]
            data = i.split(' ')[1]
            if data_type == 'g':
                DATA = data.split(',')
                node_num = int(DATA[0])
                edge_num = int(DATA[1])
            if data_type == 'n':
                DATA = data.split(',')
                no = int(DATA[0])
                a_v = float(DATA[10][:-2])
                node_data[no] = {'a_v':a_v}
            if data_type == 'e':
                f.write(i)
            if data_type == 'X':
                DATA = data.split('_')
                day = int(DATA[0])
                cost = int(DATA[1])
                _lv = int(DATA[2])
                if day > 0:
                    break
                if _lv == RL_lv:
                    Record_X.append(i)
                    users = DATA[4].split(',')
                    # Calculate the sum of a_v
                    I = 0
                    for u in users:
                        a_v = node_data[int(u)]['a_v']
                        I += a_v
                    Queue.append((X_count,round(I,4)))
                    D_cost.append( cost * level_convert[RL_lv] )
                    X_count += 1
    f.close()


    # Sorted by I
    Queue.sort(key=lambda tup: tup[1],reverse = True)
    q = math.ceil(RL_T/2)
    b1 = budget / q

    if (b1 < 0.2 and RL_lv == 1) or (b1 < 0.8 and RL_lv == 2) or (b1 < 1 and RL_lv ==3):
        b1 = budget
        X1 = []
        while b1 > 0 and Queue != []:
            Dtop = Queue[0]
            Dtop_cost = D_cost[Dtop[0]]
            while Dtop_cost > b1 and Queue != []:
                Queue.remove(Queue[0])
                if Queue != []:
                    Dtop = Queue[0]
                    Dtop_cost = D_cost[Dtop[0]]
                else:
                    break
            if Dtop_cost > b1:
                break
            if Queue == []:
                break
            X1.append(Record_X[Dtop[0]])
            b1 -= Dtop_cost
            Queue.remove(Queue[0])

        with open(file_name,'a') as f:
            for x in X1:
                f.write(x[:2] + str(0) + x[3:])

        f.close()

        groups_num = len(X1)
        f = open(file_name, "r")
        contents = f.readlines()
        f.close()

        contents.insert(0, 'g {},{},{}\n'.format(node_num,edge_num,groups_num))

        f = open(file_name, "w")
        contents = "".join(contents)
        f.write(contents)
        f.close()
        print("Done.")

    else:
        X1 = []
        while b1 > 0 and Queue != []:
            Dtop = Queue[0]
            Dtop_cost = D_cost[Dtop[0]]
            while Dtop_cost > b1 and Queue != []:
                Queue.remove(Queue[0])
                if Queue != []:
                    Dtop = Queue[0]
                    Dtop_cost = D_cost[Dtop[0]]
                else:
                    break
            if Dtop_cost > b1:
                break
            if Queue == []:
                break
            X1.append(Record_X[Dtop[0]])
            b1 -= Dtop_cost
            Queue.remove(Queue[0])

        _groups_num = len(X1)
        groups_num = 0
        with open(file_name,'a') as f:
            for t in range(0,RL_T,2):
                for x in X1:
                    f.write(x[:2] + str(t) + x[3:])
                groups_num += _groups_num
        f.close()

        f = open(file_name, "r")
        contents = f.readlines()
        f.close()

        contents.insert(0, 'g {},{},{}\n'.format(node_num,edge_num,groups_num))

        f = open(file_name, "w")
        contents = "".join(contents)
        f.write(contents)
        f.close()


        print("Done.")