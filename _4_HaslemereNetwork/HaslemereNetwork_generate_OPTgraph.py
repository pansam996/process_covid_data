import os
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt

def generate_groups(OPT_groups,OPT_least_num_of_users):
    # generate groups
    print("Generating groups...")
    _groups = []
    for group_num in range(OPT_groups):
        user_num = random.randint(OPT_least_num_of_users,469)
        print("Group_", group_num," :",user_num)
        users = np.random.randint(0,468,size = user_num)
        cost = math.ceil(user_num / 50)
        _groups.append('{}_{}'.format(cost, ",".join(str(i) for i in users)))
    return _groups
def PowerSetsBinary(elements,budget,cost,T):
    with open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(T) + '_b='+ str(budget) +'.txt','a') as f:
        N = len(elements)
        S_set = set()
        for i in range(2 ** N):
            combo = []
            b = budget
            for j in range(N):
                if(i>>j)%2:
                    b -= cost[j]
                    if b >= 0:
                        combo.append(elements[j])
                    else:
                        break
            if combo != []:
                S_set.add(tuple(combo))

        write_x = 0
        count_tup = 0
        for tup in S_set:
            for s in tup:
                f.write('{}'.format(s))
                write_x += 1
            f.write('* {}\n'.format(count_tup))
            count_tup += 1
    f.close()

    f = open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(T)+ '_b='+ str(budget) +'.txt', "r")
    contents = f.readlines()
    f.close()

    tmp = contents[0].split(',')

    contents[0] = tmp[0] + "," + tmp[1] + "," + str(write_x) + '\n'

    f = open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(T)+ '_b='+ str(budget) +'.txt', "w")
    contents = "".join(contents)
    f.write(contents)
    f.close()

def HaslemereNetwork_generate_OPTgraph(OPT_T, OPT_groups, OPT_budget, OPT_node_parameter, OPT_lv, OPT_least_num_of_users):
    t_len = len(OPT_T)
    b_len = len(OPT_budget)
    #### read the data
    data = pd.read_csv('./_4_HaslemereNetwork/Kissler_DataS1.csv',header=None,sep=',')

    # column info
    time = data[:][0] # timeslot
    id_1 = data[:][1] # userid 1
    id_2 = data[:][2] # userid 2
    distance = data[:][3] # distance

    #### add the mathc nodes and edges
    match_nodes = []
    edge_list = []
    threshold = 50

    for i in range(len(data)):
        if distance[i] <= threshold:
            match_nodes.append(id_1[i])
            match_nodes.append(id_2[i])
            edge_list.append([id_1[i],id_2[i]])

    G = nx.Graph()
    # add node
    for i in range(len(match_nodes)):
        G.add_node(match_nodes[i])

    # add edge
    for i in range(len(edge_list)):
        G.add_edge(edge_list[i][0],edge_list[i][1])

    print('Edge number :',G.number_of_edges(),'\nNode number :',G.number_of_nodes())

    all_edge = list(G.edges)
    all_edge.sort()

    all_node = list(G.nodes)
    all_node.sort()

    # a_v probability generate
    a_v_mode = OPT_node_parameter[8]
    a_v = []
    if a_v_mode:
        mean = OPT_node_parameter[9]
        var = OPT_node_parameter[10]
        for i in range(len(all_edge)):
            prob = round(np.random.normal(mean,scale=var),4)
            a_v.append(prob)

    else:
        for i in range(len(all_edge)):
            prob = round(random.uniform(0,1),4)
            a_v.append(prob)

    #generate groups
    _groups = generate_groups(OPT_groups,OPT_least_num_of_users)

    for t_idx in range(t_len):
        for b_idx in range(b_len):
            #### Graph.txt
            with open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(OPT_T[t_idx])+ '_b='+ str(OPT_budget[b_idx]) +'.txt','w') as f:
                f.write('{} {},{},{}\n'.format('g', G.number_of_nodes(), G.number_of_edges(), OPT_T[t_idx] * OPT_groups * OPT_lv))
            f.close()

            #### Node
            with open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(OPT_T[t_idx])+ '_b='+ str(OPT_budget[b_idx]) +'.txt','a') as f:
                for i in all_node:
                    node_type = random.randint(0,3)
                    R = OPT_node_parameter[0]
                    Ct = OPT_node_parameter[1]
                    S = OPT_node_parameter[2]
                    Cr = OPT_node_parameter[3]
                    HI = OPT_node_parameter[4]
                    HA = OPT_node_parameter[5]
                    HT = OPT_node_parameter[6]
                    D = OPT_node_parameter[7]
                    f.write('{} {},{},{},{},{},{},{},{},{},{},{}\n'.format('n',i - 1, node_type, R, Ct, S, Cr, HI, HA, HT, D,a_v[i-1]))
            f.close()


            #### Edge
            with open('./_4_HaslemereNetwork/graph/OPTgraph'+ '_T=' + str(OPT_T[t_idx])+ '_b='+ str(OPT_budget[b_idx]) +'.txt','a') as f:
                for i in range(len(all_edge)):
                    prob = random.uniform(0,1)
                    node1 = all_edge[i][0] - 1
                    node2 = all_edge[i][1] - 1
                    f.write('{} {},{},{}\n'.format('e',node1,node2,round(prob,3)))
            f.close()


            process_data = []
            data_cost = []
            level_convert = {1:0.5 ,2:1}
            for d in range(OPT_T[t_idx]):
                for idx in range(len(_groups)):
                    tmp = _groups[idx].split('_')
                    g = tmp[1]
                    cost = tmp[0]
                    eta = -1
                    for l in range(OPT_lv):
                        process_data.append('{} {}_{}_{}_{}_{}\n'.format('S', d, cost, l+1, eta, g))
                        data_cost.append(level_convert[l+1] * float(cost))

            print("Generating OPTgraph_T=",str(OPT_T[t_idx]),"_b=",str(OPT_budget[b_idx]))
            PowerSetsBinary(process_data,OPT_budget[b_idx],data_cost,OPT_T[t_idx])
    print("Done.")




