import os
import numpy as np
import pandas as pd
import random
import math
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
import networkx as nx
import matplotlib.pyplot as plt

def HaslemereNetwork_generate_groups(groups,least_num_of_users,basic_num):
    threshold = 50
    #### read the data
    data = pd.read_csv('./_4_HaslemereNetwork/Kissler_DataS1.csv',header=None,sep=',')
    # column info
    time = data[:][0] # timeslot
    id_1 = data[:][1] # userid 1
    id_2 = data[:][2] # userid 2
    distance = data[:][3] # distance

    #### add the mathc nodes and edges
    match_nodes = []
    edge_list = []

    for i in range(len(data)):
        if distance[i] <= threshold:
            match_nodes.append(id_1[i])
            match_nodes.append(id_2[i])
            edge_list.append([id_1[i],id_2[i]])

    G = nx.Graph()
    # add node
    for i in range(len(match_nodes)):
        G.add_node(match_nodes[i])

    # add edge
    for i in range(len(edge_list)):
        G.add_edge(edge_list[i][0],edge_list[i][1])

    all_edge = list(G.edges)
    all_edge.sort()

    all_node = list(G.nodes)
    all_node.sort()

    # generate 100 groups
    with open('./_4_HaslemereNetwork/HaslemereNetwork_100_groups','w') as f:
        for group_num in range(groups):
            user_num = random.randint(least_num_of_users,469)
            print("Group_", group_num," :",user_num)
            users = np.random.randint(0,G.number_of_nodes()-1,size = user_num)
            cost = math.ceil(user_num / basic_num)
            f.write('{}_{}\n'.format(cost, ",".join(str(i) for i in users)))


def HaslemereNetwork_gnenrate_plot_graphs():
    threshold = 50
    #### read the data
    data = pd.read_csv('./_4_HaslemereNetwork/Kissler_DataS1.csv',header=None,sep=',')
    # column info
    time = data[:][0] # timeslot
    id_1 = data[:][1] # userid 1
    id_2 = data[:][2] # userid 2
    distance = data[:][3] # distance

    #### add the mathc nodes and edges
    match_nodes = []
    edge_list = []

    for i in range(len(data)):
        if distance[i] <= threshold:
            match_nodes.append(id_1[i])
            match_nodes.append(id_2[i])
            edge_list.append([id_1[i],id_2[i]])

    G = nx.Graph()
    # add node
    for i in range(len(match_nodes)):
        G.add_node(match_nodes[i])

    # add edge
    for i in range(len(edge_list)):
        G.add_edge(edge_list[i][0],edge_list[i][1])
    plt.figure(1,figsize=(8,8))
    nx.draw_networkx_nodes(G,pos=nx.random_layout(G),node_size=20)
    plt.savefig('./_4_HaslemereNetwork/HaslemereNetwork.png')